FROM python:3.6-alpine

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app/

RUN apk --update add python py-pip openssl ca-certificates py-openssl wget && \
		apk --update add --virtual build-dependencies libffi-dev openssl-dev python-dev py-pip build-base && \
		pip3 install --upgrade pip && \
		pip3 install --no-cache-dir -r requirements.txt && \
		apk del build-dependencies

COPY . /usr/src/app

EXPOSE 9092

ENTRYPOINT ["python3"]

CMD ["-m", "known_pose_service"]
