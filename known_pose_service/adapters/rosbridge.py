import logging

from time import sleep
from roslibpy import Ros, Service, ServiceRequest, ServiceResponse
from known_pose_service.models.basic_pose import BasicPose


logging.basicConfig(format='[%(levelname)s] -%(asctime)s: %(message)s', level=logging.INFO)

client = Ros(host='rosbridge-server', port=9093)

# def create_ros_client():
#   return Ros(host='rosbridge-server', port=9090)


def ros_connected() -> bool:
    client.on_ready(lambda:
                    logging.info('ROS connected: ', client.is_connected))
    if (client.is_connected):
        return True
    else:
        return False


def get_current_robot_pose() -> BasicPose:
    client.run()
    service = Service(client,
                      '/accuracy/get_robot_pose',
                      'accuracy/GetRobotPose')
    request = ServiceRequest()
    result: ServiceResponse = service.call(request)
    return create_pose_from_dict(result['pose'])


def create_reference_scan(scan_seconds: float = 0.25) -> str:
    service = Service(client,
                      '/accuracy/create_reference_scan',
                      'accuracy/CreateReferenceScan')
    request = ServiceRequest({
        'scan_seconds': scan_seconds
    })
    result: ServiceResponse = service.call(request)
    logging.info('Service response: {}'.format(result['scan']))
    return str(result['scan'])


def create_pose_from_dict(ros_message) -> BasicPose:
    logging.info('Robot Pose: '
                 + str(ros_message['position'])
                 + str(ros_message['orientation']))
    return BasicPose(
        name='Current Pose',
        position=ros_message['position'],
        orientation=ros_message['orientation']
    )
