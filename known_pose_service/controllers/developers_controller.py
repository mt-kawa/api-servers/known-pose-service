import connexion
import six
import os
import uuid
import json
import re
import logging

from typing import List
from datetime import datetime

from known_pose_service.models.accurate_pose_array import AccuratePoseArray  # noqa: E501
from known_pose_service.models.basic_pose import BasicPose  # noqa: E501
from known_pose_service.models.accurate_pose import AccuratePose  # noqa: E501
from known_pose_service.models.inaccurate_pose import InaccuratePose  # noqa: E501
from known_pose_service.models.inaccurate_pose_array import InaccuratePoseArray  # noqa: E501
from known_pose_service.models.inline_response200 import InlineResponse200  # noqa: E501
from known_pose_service import util
from known_pose_service.adapters.rosbridge import get_current_robot_pose, create_reference_scan


logging.basicConfig(format='[%(levelname)s] -%(asctime)s: %(message)s', level=logging.INFO)
path_to_scans = os.path.dirname(__file__) + '/../data'
path_to_poses = path_to_scans + '/poses.json'

if (not os.path.isfile(path_to_poses)):
    with open(path_to_poses, mode='w', encoding='utf-8') as datastore:
        schema = {
            'schema': 'Known Pose',
            'version': '1.1',
            'Author': 'Kai Waelti',
            'known_poses': {
                'accurate': [],
                'inaccurate': [],
            }
        }
        json.dump(schema, datastore, indent=2, default=str)
        logging.info('Created json datastore at ' + path_to_poses)


def add_accurate_pose(body=None) -> str:  # noqa: E501
    """adds an accurate pose item

    Adds an accurate pose to the system. Just give the Pose a name.  # noqa: E501

    :param body: Optional pose item to add
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        conform_pose = BasicPose.from_dict(connexion.request.get_json())  # noqa: E501
        try:
            logging.info('I received the following name: \n'
                         + str(conform_pose.name))
            if (pose_already_exists(str(conform_pose.name))):
                return 'Pose with same name already exists. Nothing added.'
            accurate_pose: AccuratePose = create_accurate_pose(str(conform_pose.name))
            logging.info('Creating accurate pose '
                         + str(accurate_pose.inaccurate_pose.dns)
                         + ' with uuid5 <'
                         + str(accurate_pose.inaccurate_pose.id) + '>')
            add_accurate_pose_to_datastore(accurate_pose)
            return 'AccuratePose ' + str(conform_pose.name) + ' added to datastore.'
        except ValueError as e:
            logging.error('Could not convert data to an Pose object.' + str(e))
            return 'Conversion failed - check validity of your Pose object'
        except FileNotFoundError as e:
            logging.error('Could not find data file.' + str(e))
            return 'Datastore not found - created a new one'
        except Exception as e:
            logging.error('Some error occured: ' + str(e))
            return 'Some error has occured -> check the server logs for more info!'
    else:
        logging.warning('Could not read JSON object (malformed). Received: \n'
                        + body)
        return 'Malformed JSON object - please check validity'


def add_inaccurate_pose(body=None) -> str:  # noqa: E501
    """adds an inaccurate pose item

    Adds an inaccurate pose to the system. Just give the Pose a name and optionally a Pose in the map to use.  # noqa: E501

    :param body: Optional pose item to add
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        conform_pose = BasicPose.from_dict(connexion.request.get_json())  # noqa: E501
        try:
            logging.info('I received the following name: \n'
                         + str(conform_pose.name))
            if (pose_already_exists(str(conform_pose.name))):
                return 'Pose with same name already exists. Nothing added.'
            inaccurate_pose = create_inaccurate_pose(conform_pose.name)
            logging.info('Creating inaccurate pose '
                         + inaccurate_pose.dns
                         + ' with uuid5 <'
                         + inaccurate_pose.id + '>')
            add_inaccurate_pose_to_datastore(inaccurate_pose)
            return 'InaccuratePose ' + conform_pose.name + ' added to datastore.'
        except ValueError as e:
            logging.error('Could not convert data to an Pose object.' + str(e))
            return 'Conversion failed - check validity of your Pose object'
        except FileNotFoundError as e:
            logging.error('Could not find data file.' + str(e))
            return 'Datastore not found - created a new one'
        except Exception as e:
            logging.error('Some error occured: ' + str(e))
            return 'Some error has occured -> check the server logs for more info!'
    else:
        logging.warning('Could not read JSON object (malformed). Received: \n'
                        + body)
        return 'Malformed JSON object - please check validity'


def poses_id_delete(id_: str) -> str:  # noqa: E501
    """deletes a pose item

    Deletes an accurate or inaccurate pose from the system using the &#x60;uuid&#x60;.  # noqa: E501

    :param id: uuid
    :type id: str

    :rtype: InlineResponse200
    """
    deleted = False
    if (id_ is None):
        return deleted
    with open(path_to_poses) as json_file:
        json_data = json.load(json_file)
        inaccurate_pose_array = json_data['known_poses']['inaccurate']
        for index, db_pose in enumerate(inaccurate_pose_array):
            inaccurate_pose = InaccuratePose.from_dict(db_pose)  # noqa: E501
            if (inaccurate_pose.id == id_):
                del inaccurate_pose_array[index]
                deleted = True
        accurate_pose_array = json_data['known_poses']['accurate']
        for index, db_pose in enumerate(accurate_pose_array):
            accurate_pose = AccuratePose.from_dict(db_pose)  # noqa: E501
            if (accurate_pose.inaccurate_pose.id == id_):
                del accurate_pose_array[index]
                deleted = True
        with open(path_to_poses, mode='w', encoding='utf-8') as json_file:
            json.dump(json_data, json_file, indent=2, default=str)
            logging.info('Pose with ID '
                         + id_
                         +' was successfully deleted from datastore')
    return deleted


def search_accurate_pose(id_=None, search_string=None, skip=None, limit=None) -> AccuratePoseArray:  # noqa: E501
    """searches accurate pose

    By passing in the appropriate options, you can search for defined accurate semantic poses in the environment  # noqa: E501

    :param id: optional uuid of a single accurate pose to be returned
    :type id: str
    :param search_string: pass an optional search string for looking up a pose
    :type search_string: str
    :param skip: number of records to skip for pagination
    :type skip: int
    :param limit: maximum number of records to return
    :type limit: int

    :rtype: AccuratePoseArray
    """
    accurate_pose_list: List[InaccuratePose] = []
    with open(path_to_poses) as json_file:
        json_data = json.load(json_file)
        for db_pose in json_data['known_poses']['accurate']:
            accurate_pose = AccuratePose.from_dict(db_pose)  # noqa: E501
            accurate_pose_list.append(accurate_pose)
    info_message = 'Client searching for poses'
    if id_ is not None:
        logging.info(info_message + ' with ID=<' + id_ + '>')
        for i in range(0, len(accurate_pose_list)):
            if id_ == accurate_pose_list[i].inaccurate_pose.id:
                return AccuratePoseArray([accurate_pose_list[i]])
    try:
        result = accurate_pose_list
        if search_string is not None:
            filtered_result: List[AccuratePose] = []
            for i in range(0, len(result)):
                if re.search(search_string, result[i].name, re.IGNORECASE):
                    filtered_result.append(result[i])
            info_message += ' - containing <' + search_string + '> in the name'
            result = filtered_result
        if skip is not None:
            paginated_result: List[AccuratePose] = []
            for i in range(skip, len(result)):
                paginated_result.append(result[i])
            result = paginated_result
            info_message += ' - skipping the first ' + str(skip) + ' elements'
        if limit is not None:
            limited_results: List[AccuratePose] = []
            for i in range(0, limit):
                limited_results.append(result[i])
            result = limited_results
            info_message += ' - limiting the response to ' + str(limit) + ' elements'
    except IndexError as e:
        logging.error('Search was too restrictive to return anything.' + e)
        return 'Search too restrictive - try adapting the parameters.'

    logging.info(info_message + '.')
    return AccuratePoseArray(result)


def search_pose(id=None, search_string=None, skip=None, limit=None) -> InaccuratePoseArray:  # noqa: E501
    """searches pose

    By passing in the appropriate options, you can search for defined semantic poses in the environment  # noqa: E501

    :param id: optional uuid of a single pose to be returned
    :type id: str
    :param search_string: pass an optional search string for looking up a pose
    :type search_string: str
    :param skip: number of records to skip for pagination
    :type skip: int
    :param limit: maximum number of records to return
    :type limit: int

    :rtype: InaccuratePoseArray
    """
    inaccurate_pose_list: List[InaccuratePose] = []
    with open(path_to_poses) as json_file:
        json_data = json.load(json_file)
        for db_pose in json_data['known_poses']['inaccurate']:
            inaccurate_pose = InaccuratePose.from_dict(db_pose)  # noqa: E501
            inaccurate_pose_list.append(inaccurate_pose)
        for db_pose in json_data['known_poses']['accurate']:
            accurate_pose = AccuratePose.from_dict(db_pose)  # noqa: E501
            inaccurate_pose_list.append(accurate_pose.inaccurate_pose)

    info_message = 'Client searching for poses'
    result = inaccurate_pose_list
    # if id is not None:
    #     for i in range(0, len(result)):
    #         if re.search(id, result[i].id, re.IGNORECASE):
    #             result = [result[i]]
    # try:
    #     if search_string is not None:
    #         filtered_result: List[InaccuratePose] = []
    #         for i in range(0, len(result)):
    #             if re.search(search_string, result[i].name, re.IGNORECASE):
    #                 filtered_result.append(result[i])
    #         info_message += ' - containing <' + search_string + '> in the name'
    #         result = filtered_result
    #     if skip is not None:
    #         paginated_result: List[InaccuratePose] = []
    #         for i in range(skip, len(result)):
    #             paginated_result.append(result[i])
    #         result = paginated_result
    #         info_message += ' - skipping the first ' + str(skip) + ' elements'
    #     if limit is not None:
    #         limited_results: List[InaccuratePose] = []
    #         for i in range(0, limit):
    #             limited_results.append(result[i])
    #         result = limited_results
    #         info_message += ' - limiting the response to ' + str(limit) + ' elements'
    # except IndexError as e:
    #     logging.error('Search was too restrictive to return anything.' + e)
    #     return 'Search too restrictive - try adapting the parameters.'
    #
    # logging.info(info_message + '.')
    return InaccuratePoseArray(result)


"""
A unique ID for a new Pose is generated by uuid5 using a SHA-1 hash of a
namespace and a name
"""
def generate_id_from(pose_name: str) -> str:
    pose_dns = generate_dns_from(pose_name)
    pose_uuid = str(uuid.uuid5(uuid.NAMESPACE_DNS, 'known_pose.' + pose_dns))
    return pose_uuid

def generate_dns_from(pose_name: str) -> str:
    return pose_name.replace(' ', '-').lower()

def pose_already_exists(pose_name: str) -> bool:
    with open(path_to_poses) as json_file:
        json_data = json.load(json_file)
        known_pose_array = json_data['known_poses']
        accurate_pose_array = known_pose_array['accurate']
        inaccurate_pose_array = known_pose_array['inaccurate']
        if (len(inaccurate_pose_array) == 0 and len(accurate_pose_array) == 0):
            return False
        if any(pose['inaccurate_pose']['basic_pose']['name'] == pose_name for pose in accurate_pose_array):
            logging.info('AccuratePose with name <' + pose_name + '> already exists.')
            return True
        if any(pose['basic_pose']['name'] == pose_name for pose in inaccurate_pose_array):
            logging.info('InaccuratePose with name <' + pose_name + '> already exists.')
            return True
        else:
            return False

def has_pose(pose: BasicPose) -> bool:
    if (pose.position() is not None or pose.orientation() is not None):
        return True
    else:
        return False

def create_inaccurate_pose(pose_name: str) -> InaccuratePose:
    robot_pose: BasicPose = get_current_robot_pose()
    robot_pose.name = pose_name
    return InaccuratePose(
        id=generate_id_from(pose_name),
        dns=generate_dns_from(pose_name),
        basic_pose=robot_pose,
        is_accurate=False,
        taken_at=datetime.now()
    )

def create_accurate_pose(pose_name: str) -> AccuratePose:
    accurate_pose = AccuratePose(
        inaccurate_pose=create_inaccurate_pose(pose_name),
        reference_scan=str(create_reference_scan(scan_seconds=0.25))
    )
    accurate_pose.inaccurate_pose.is_accurate = True
    return accurate_pose

def add_inaccurate_pose_to_datastore(pose: InaccuratePose) -> bool:
    with open(path_to_poses) as json_file:
        json_data = json.load(json_file)
        inaccurate_pose_array = json_data['known_poses']['inaccurate']
        inaccurate_pose_array.append(pose.to_dict())
        with open(path_to_poses, mode='w', encoding='utf-8') as json_file:
            json.dump(json_data, json_file, indent=2, default=str)
            logging.info('Pose '
                         + pose.basic_pose.name
                         + ' was added successfully to the datastore')
            return True
        return False

def add_accurate_pose_to_datastore(pose: AccuratePose) -> bool:
    with open(path_to_poses) as json_file:
        json_data = json.load(json_file)
        accurate_pose_array = json_data['known_poses']['accurate']
        accurate_pose_array.append(pose.to_dict())
        with open(path_to_poses, mode='w', encoding='utf-8') as json_file:
            json.dump(json_data, json_file, indent=2, default=str)
            logging.info('Pose '
                         + str(pose.inaccurate_pose.basic_pose.name)
                         + ' was added successfully to the datastore')
            return True
        return False
